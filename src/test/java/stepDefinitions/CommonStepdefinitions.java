package stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.support.PageFactory;
import pages.PageforCommonElements;
import utils.BaseClass;

public class CommonStepdefinitions extends BaseClass {
PageforCommonElements pageforCommonElements;
    public CommonStepdefinitions() {
        pageforCommonElements=new PageforCommonElements();
    }
    @Given("^Launch browser$")
    public void launch_browser() throws Throwable {
        driver.navigate().to("http://automationexercise.com");
    }

//    @Given("^Launch browser$")
//    public void launch_browser(){
//        System.out.println("Prakash khulbey");
//    }

    @Given("^Navigate to url 'http://automationexercise\\.com'$")
    public void navigate_to_url_http_automationexercise_com() throws Throwable {
      //  driver.get("http://automationexercise.com");
    }

    @When("^Click 'Continue' button$")
    public void click_Continue_button() throws Throwable {
        pageforCommonElements.clickAccountCreatedPageContinueButton();
    }
}
